# Policy ID: 001

## BSI-Anforderungen

### SYS.1.6.A15
*Softwarebetreiber* **muss** ​​die angegebenen Begrenzungen der Ressourcen (Limits) durch entsprechende Konfiguration der Container umsetzen.


## Formale Beschreibung

Eine Policy muss sicherstellen, dass Ressourcenbegrenzungen ([Requests und Limits](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)) für alle im Cluster laufenden Container existieren.

## Technische Umsetzungsvorschläge (optional)

- Admission Control (statische Prüfung auf Pod .spec.containers.resources.requests/limits)

## Technische Umsetzungen

- Kyverno: [IG BvC Implementierung](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-kyverno/-/blob/master/policies/require-limits-and-requests.yaml)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: [Standardmäßig implementiert](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/no_resources_specified.json)
