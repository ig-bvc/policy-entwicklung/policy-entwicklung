# Policy ID: 019

## BSI-Anforderungen

### APP.4.4.A9
*Softwarebetreiber* **SOLLTE** ​Anforderungen des Softwarelieferanten zur Nutzung des default-Service-Accounts ablehnen.

## Formale Beschreibung

Die Nutzung des "default"-Service-Accounts eines Namespaces sollte unterbunden werden.

## Technische Umsetzungsvorschläge (optional)

- AdmissionControl in Pod: Prüfung auf "serviceAccountName"

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox:
  - [Standardmäßig implementiert - Automount default Serviceaccounttoken](https://github.com/stackrox/stackrox/blob/master/pkg/defaults/policies/files/automount_service_account_token.json)
  - [IG BvC - Automount default Serviceaccounttoken](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-automount-satoken.json)
  - [IG BvC - Default Serviceaccount](https://gitlab.opencode.de/ig-bvc/policy-entwicklung/richtlinien-umsetzung-stackrox/-/blob/main/policies/disallow-default-serviceaccount.json)
